package com.bootincat.uhfreader.models;

import android.content.Context;
import android.os.RemoteException;

import androidx.annotation.NonNull;

import com.bootincat.uhfreader.models.interfaces.AbstractUHFModel;
import com.olc.uhf.tech.IUhfCallback;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.TimerTask;

public final class UhfReadModel extends AbstractUHFModel {

    public UhfReadModel(@NonNull Context ctx) {
        super(ctx);
    }

    private UhfSearchTask mPeriodicSearchTask;

    @Override
    public void requestSearchRFIDTags(final int interval) {
        mPeriodicSearchTask = new UhfSearchTask();

        mTimer.schedule(mPeriodicSearchTask, 0, interval);

        super.searchStarted();
    }

    @Override
    public void cancelSearchRFIDTags() {
        mTimer.cancel();

        super.searchFinished(mPeriodicSearchTask.getFoundTags());
    }

    protected class UhfSearchTask extends TimerTask {

        private IUhfCallback mUhfCallback;

        private HashSet<String> mUniqueFoundTags;

        UhfSearchTask() {

            mUniqueFoundTags = new HashSet<>();

            mUhfCallback = new IUhfCallback.Stub() {
                @Override
                public void doInventory(List<String> list) throws RemoteException {

                    for (String rfidTag : list) {

                        String strEpc = rfidTag.substring(2, 6) + rfidTag.substring(6);

                        if (mUniqueFoundTags.contains(strEpc)) {
                            continue;
                        }

                        mUniqueFoundTags.add(strEpc);

                        UhfReadModel.super.newItemFound(strEpc);
                    }
                }

                @Override
                public void doTIDAndEPC(List<String> list) throws RemoteException { }
            };
        }

        @Override
        public void run() {
            mUhfISO6c.inventory(mUhfCallback);
        }

        ArrayList<String> getFoundTags() {
            return new ArrayList<>(mUniqueFoundTags);
        }


    }
}
