package com.bootincat.uhfreader;

import androidx.appcompat.app.AppCompatActivity;

import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ProgressBar;
import android.widget.Switch;

import com.bootincat.uhfreader.models.UhfReadModel;
import com.bootincat.uhfreader.models.interfaces.OnSearchEventListener;

import java.util.ArrayList;


public class MainActivity extends AppCompatActivity {

    private Switch mSearchSwitch;

    private ProgressBar mProgressBar;

    private UhfReadModel mUhfReadModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        initView();

        mUhfReadModel = new UhfReadModel(getApplicationContext());

        mUhfReadModel.setOnSearchEventListener(new OnSearchEventListener<String>() {
            @Override
            public void onSearchStarted() {

            }

            @Override
            public void onNewItemFound(String foundItem) {
                Log.d("YO", "FOUND NEW UNIQUE ITEM: " + foundItem);
            }

            @Override
            public void onSearchFinished(ArrayList<String> resultList) {
                for (String str : resultList) {
                    Log.d("YO", "RESULT: " + str);
                }
            }
        });

        mSearchSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    mUhfReadModel.requestSearchRFIDTags(500);
                } else {
                    mUhfReadModel.cancelSearchRFIDTags();

                    mProgressBar.setIndeterminate(false);
                }
            }
        });
    }

    private void initView() {
        mSearchSwitch = findViewById(R.id.searchSwitch);

        mProgressBar = findViewById(R.id.searchProgressBar);
    }
}
