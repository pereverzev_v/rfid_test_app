package com.bootincat.uhfreader.models.interfaces;

import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.olc.uhf.UhfAdapter;
import com.olc.uhf.UhfManager;
import com.olc.uhf.tech.ISO1800_6C;

import java.util.ArrayList;
import java.util.Timer;

public abstract class AbstractUHFModel<T> {

    protected UhfManager mUhfManager;

    protected ISO1800_6C mUhfISO6c;

    protected Context mContext;

    protected Timer mTimer;

    private OnSearchEventListener mListener;

    public AbstractUHFModel(@NonNull final Context ctx) {
        mContext = ctx;

        mUhfManager = UhfAdapter.getUhfManager(mContext);

        mUhfManager.open();

        mUhfISO6c = mUhfManager.getISO1800_6C();

        mTimer = new Timer();
    }

    protected String logTag() {
        return this.getClass().getCanonicalName();
    }

    public void setOnSearchEventListener(@Nullable OnSearchEventListener listener) {
        mListener = listener;
    }

    protected void searchStarted() throws NullPointerException {
        try {
            mListener.onSearchStarted();

        } catch (NullPointerException e) {
            Log.e(logTag(), e.toString());

            e.printStackTrace();
        }
    }

    protected void searchFinished(ArrayList<T> resultList) throws NullPointerException {
        try {
            mListener.onSearchFinished(resultList);

        } catch (NullPointerException e) {
            Log.e(logTag(), e.toString());

            e.printStackTrace();
        }
    }

    protected void newItemFound(T foundItem) throws NullPointerException {
        try {
            mListener.onNewItemFound(foundItem);

        } catch (NullPointerException e) {
            Log.e(logTag(), e.toString());

            e.printStackTrace();
        }
    }

    public abstract void requestSearchRFIDTags(final int interval);

    public abstract void cancelSearchRFIDTags();
}
