package com.bootincat.uhfreader.models.interfaces;

import java.util.ArrayList;

public interface OnSearchEventListener<T> {
    void onSearchStarted();
    void onNewItemFound(T foundItem);
    void onSearchFinished(ArrayList<T> resultList);
}
